# CodeMonk-Assignment



## Explanation

For Task2 I did some EDA, and created a custom shufflenet model
for multilabel classification with softmax, to avoid class imbalance problem, Here I have used weighted loss fuction
by passing weights to crossentropy loss, I know this model will
perform very worst, because I downscaled image into 64X64 and 
trained with 10 epoch, because my GPU Quota in Kaggle is Over, 
And in Testing I didn't included test images, Unfortunately
I deleted that, if you want to test please create test_images directory and store some images and Run Testing.ipynb

I did task1 EDA as well
